NIKUE Kouete Madjé - TPA

Animations apportées à la page d'accueil: 

* Le menu grossit se déplace de 2rem et prend la couleur bleu au survole du curseur (Hover);

* un objet comportant une image de roket svg, survole la page accueil , fait trois (03) tours de la page et disparaît;

* A sa disparition , la roket svg retombe sur la page.

* Les images de la page se déplacent en scroll translation à l'activation de la page d'accueil

* Un bouton " Call to action " qui ouvre, vers le bas de la page, un formulaire de nom et prenom, adresse courriel et bouton envoyé